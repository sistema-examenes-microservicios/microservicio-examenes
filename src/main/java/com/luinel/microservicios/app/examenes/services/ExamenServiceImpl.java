package com.luinel.microservicios.app.examenes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luinel.microservicios.app.examenes.models.repository.AsignaturaRepository;
import com.luinel.microservicios.app.examenes.models.repository.ExamenRepository;
import com.luinel.microservicios.commons.examenes.models.entity.Asignatura;
import com.luinel.microservicios.commons.examenes.models.entity.Examen;
import com.luinel.microservicios.commons.services.CommonServiceImpl;

@Service
public class ExamenServiceImpl extends CommonServiceImpl<Examen, ExamenRepository> implements ExamenService {

	@Autowired
	private AsignaturaRepository asignaturaRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Examen> findByNombre(String term) {
		return repository.findByNombre(term);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Asignatura> findAllAsignaturas() {
		return asignaturaRepository.findAll();
	}

	@Override
	@Transactional
	public Iterable<Long> findExamenesIdConRespuestasByPreguntaIds(Iterable<Long> preguntaIds) {
		return repository.findExamenesIdConRespuestasByPreguntaIds(preguntaIds);
	}

}

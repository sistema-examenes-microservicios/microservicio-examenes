package com.luinel.microservicios.app.examenes.services;

import java.util.List;

import com.luinel.microservicios.commons.examenes.models.entity.Asignatura;
import com.luinel.microservicios.commons.examenes.models.entity.Examen;
import com.luinel.microservicios.commons.services.CommonService;

public interface ExamenService extends CommonService<Examen>{

	public List<Examen> findByNombre(String term);
	
	public Iterable<Asignatura> findAllAsignaturas();
	
	public Iterable<Long> findExamenesIdConRespuestasByPreguntaIds(Iterable<Long> preguntaIds);
}
